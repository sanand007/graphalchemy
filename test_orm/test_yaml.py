from src.graphalchemy.read_cfg import read_config
from pathlib import Path


def test_yaml_data():
    """
    test the data in db_config.yaml file
    """
    config = read_config(Path.home()/'etc'/'whai'/'config'/'db_config.yaml')
    assert config['database']['postgres']['user'] == 'root'

from setuptools import setup, find_packages

setup(
    name='graphalchemy',
    version='0.0.1',    
    description='python graph package',
    url='',
    author='Smit Anand',
    author_email='',
    license='',
    packages=find_packages(),
    install_requires=['sqlalchemy',
                      'pyyaml',
                      'networkx',
                      'Path',
                      'pandas',
                      'numpy',
                      'pytest',
                      'psycopg2',
                      'path'
    ],
)
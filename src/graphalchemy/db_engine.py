from sqlalchemy import create_engine
from src.graphalchemy.read_cfg import read_config
from pathlib import Path


def _db_engine() -> object:
    try:
        db_config = read_config(Path.home()/'etc'/'whai'/'config'/'db_config.yaml')
        return create_engine('postgresql://'
                             + db_config['database']['postgres']['user']
                             + ':'
                             + db_config['database']['postgres']['password'] + '@localhost:5432/'
                             + db_config['database']['postgres']['schema'])
    except BaseException as e:
        return e


def db_engine() -> object:
    """
    returns a database connection engine
    """
    return _db_engine()


if __name__ == '__main__':
    print(db_engine())
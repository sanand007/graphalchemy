from src.graphalchemy.read_cfg import read_config
from src.graphalchemy.read_data import read_table_data
import networkx as nx
from pathlib import Path


def _create_weighted_graph() -> nx.Graph():
    config = read_config(Path.home()/'etc'/'whai'/'config'/'db_config.yaml')
    graph_data = read_table_data(config['database']['postgres']['table']['weighted'])
    G = nx.Graph();
    for connection in graph_data:
        G.add_edge(connection[1], connection[2], weight=connection[3])
    return G


def create_weighted_graph() -> nx.Graph():
    """
    create a networkx graph with weighted data and returns the graph object
    """
    return _create_weighted_graph()


if __name__ == '__main__':
    create_weighted_graph()

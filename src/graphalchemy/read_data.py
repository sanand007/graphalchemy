import sqlalchemy as db
from src.graphalchemy.db_engine import db_engine


def _read_table_data(table_name) -> list:
    try:
        engine = db_engine()
        weighted_table = db.Table(table_name, db.MetaData(), autoload=True, autoload_with=engine)
        query = db.select([weighted_table])
        ResultProxy = engine.connect().execute(query)
        data = ResultProxy.fetchall()
        return data
    except BaseException as e:
        return e


def read_table_data(table_name) -> list:
    """
    read directed data from database and return a list containing data
    """
    return _read_table_data(table_name)


if __name__ == '__main__':
    print(read_table_data('weighted'))
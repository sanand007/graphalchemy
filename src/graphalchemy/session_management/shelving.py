import shelve
from pathlib import Path


def _store_session() -> shelve.open('filename', 'conditon'):
    filename = Path.home()/'etc'/'whai'/'session'/'session.sess'
    galchemy_session = shelve.open(filename, 'n')
    for key in dir():
        print('value ', key)
        try:
            galchemy_session[key] = globals()[key]
        except TypeError:
            #
            # __builtins__, galchemy_session, and imported modules can not be shelved.
            #
            print('ERROR shelving: {0}'.format(key))
    galchemy_session.close()


def store_session() -> shelve.open('filename', 'conditon'):
    """
    This method is used to store the session objects in shelve and
    restore later

    >>>from src.graphalchemy.session_management.shelving import store_session
    >>>store_session()
    """
    return _store_session()


if __name__ == '__main__':
    store_session()
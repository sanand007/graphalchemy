import shelve
from src.graphalchemy.read_cfg import read_config
from pathlib import Path


def _restore_session() -> shelve.open(str, str):
    config = read_config(Path.home()/'etc'/'whai'/'config'/'galchemy_config.yaml')
    filename = config.shelve_loc
    galchemy_session = shelve.open(filename)
    for key in galchemy_session:
        globals()[key] = galchemy_session[key]
    galchemy_session.close()


def restore_session() -> shelve.open(str, str):
    """
    This method is used to restore the session objects in shelve and
    ready to be used

    >>>from src.graphalchemy.session_management.unshelving import restore_session
    >>>restore_session()
    """
    return _restore_session()
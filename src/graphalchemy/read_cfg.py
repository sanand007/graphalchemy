import yaml


def _read_config(path) -> dict:
    with open(path, 'r') as db_config:
        config = yaml.safe_load(db_config)
        return config


def read_config(path) -> dict:
    """
    read configuration from yaml and returns the data dictionary
    containing configuration
    """
    return _read_config(path)
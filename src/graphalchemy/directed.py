from src.graphalchemy.read_cfg import read_config
from src.graphalchemy.read_data import read_table_data
import networkx as nx
from pathlib import Path


def _create_directed_graph() -> nx.DiGraph():
    config = read_config(Path.home()/'etc'/'whai'/'config'/'db_config.yaml')
    graph_data = read_table_data(config['database']['postgres']['table']['directed'])
    G = nx.DiGraph();
    for connection in graph_data:
        G.add_weighted_edges_from([(connection[1], connection[2], connection[3])])
    return G


def create_directed_graph() -> nx.DiGraph():
    """
    create a networkx graph with directed data and returns the graph object
    """
    return _create_directed_graph()
